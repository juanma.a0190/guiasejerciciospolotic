package guia2;
import java.util.Scanner;

public class Ejercicio4 {
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese un n�mero: ");
        int numero = s.nextInt(), ac=1;
        
        for(int i=1; i<=numero; i++){
            ac *= i;
        }
        
        System.out.println("El factorial de "+numero+" es: "+ac);
    }
    
}
