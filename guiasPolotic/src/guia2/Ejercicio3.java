package guia2;

import java.util.Arrays;
import java.util.Random;

public class Ejercicio3 {

    public static void main(String[] args) {

        int[] numeros = rellenarArray();

        System.out.println("Array original: ");
        mostrarArray(numeros);
        
        numeros = ordenarArrayAsc(numeros);
        System.out.println("Array ordenado Ascendente: ");
        mostrarArray(numeros);
        
        numeros = ordenarArrayDesc(numeros);
        System.out.println("Array ordenado Descendente: ");
        mostrarArray(numeros);

    }

    public static int[] rellenarArray() {
        Random ran = new Random();
        int[] numeros = new int[10];
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = ran.nextInt(10) + 1;
        }

        return numeros;
    }

    public static void mostrarArray(int[] numeros) {
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + " ");
        }
        System.out.println("");
    }

    public static int[] ordenarArrayAsc(int[] numeros) {
        Arrays.sort(numeros);
        return numeros;
    }

    public static int[] ordenarArrayDesc(int[] numeros) {
        int aux = 0, i = 0;

        while (i < numeros.length ) {
            for (int j = 0; j < numeros.length; j++) {
                if ((j + 1) < numeros.length) {
                    if (numeros[j] < numeros[j + 1]) {
                        aux = numeros[j];
                        numeros[j] = numeros[j + 1];
                        numeros[j + 1] = aux;
                    }
                }
            }

            i++;
        }

        return numeros;
    }

}
