package guia2;

import java.util.Scanner;

public class Ejercicio5 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Ingrese un numero:");
        int numero = s.nextInt();

        long a = 0L;
        long b = 1L;
        long acumulador = 0L;

        System.out.println("Secuencia Fibonacci:");
        for (int i = 0; i < numero; i++) {
            System.out.print(acumulador + " ");
            acumulador = a + b;
            b = a;
            a = acumulador;

        }
        System.out.println("");
    }

}
