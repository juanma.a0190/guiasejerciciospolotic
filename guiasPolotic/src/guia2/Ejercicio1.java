package guia2;

import java.util.Scanner;

public class Ejercicio1 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Ingrese el n�mero:");
        int numero = s.nextInt();
        if (numero > 0) {
            System.out.println("El resultado es:");
            for (int i = 1; i <= 10; i++) {
                System.out.println(numero + " * " + i + " = " + numero * i);
            }
        }else{
            System.out.println("Ingrese numeros positivos.");
        }

    }
}
