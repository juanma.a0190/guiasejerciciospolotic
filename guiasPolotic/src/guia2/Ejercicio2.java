package guia2;

import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String palabra = palabraIngresar(s);
        
        boolean isPalindromo = isPalindromo(palabra);
        
        if (isPalindromo) {
            System.out.println("Es palindromo");
        } else {
            System.out.println("No es palindromo");
        }
    }

    public static String palabraIngresar(Scanner s) {
        System.out.println("Ingrese la palabra:");
        String palabra = s.next();

        return palabra.toLowerCase();
    }

    public static String palabraInvertido(String palabra) {
        int j = palabra.length() - 1;
        char[] array = palabra.toCharArray();
        char[] arrayInvertido = new char[palabra.length()];

        for (int i = 0; i < palabra.length(); i++) {
            arrayInvertido[i] = array[j];
            j--;
        }

        return String.valueOf(arrayInvertido);
    }

    public static boolean isPalindromo(String palabra) {
        String palabraInvertida = palabraInvertido(palabra);
        return palabra.equals(palabraInvertida);
    }
}
