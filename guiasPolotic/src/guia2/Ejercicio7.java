package guia2;

import java.util.Scanner;

public class Ejercicio7 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        System.out.println("Elija un animal, y responda las preguntas para tratar de adivinar (Si/s) o (No/n)");
        
        String[] preguntas = {"Es herb�voro ?", "Es Mam�fero ?", "Es Dom�stico ?"};
                
        boolean[] respuestas = ingresarRespuestas(s, preguntas);
        int cantidadTrue = contarTrue(respuestas);

        System.out.println("El animal es "+obtenerAnimal(respuestas, cantidadTrue));
    }

    public static boolean[] ingresarRespuestas(Scanner s, String[] preguntas) {
        String respuesta;
        boolean[] respuestas = new boolean[preguntas.length];
        for (int i = 0; i < preguntas.length; i++) {
            System.out.println(preguntas[i]);
            respuesta = s.nextLine();
            if ("s".equalsIgnoreCase(respuesta) || "si".equalsIgnoreCase(respuesta)) {
                respuestas[i] = true;
            } else if ("n".equalsIgnoreCase(respuesta)|| "no".equalsIgnoreCase(respuesta)) {
                respuestas[i] = false;
            }else{
                System.out.println("Valor ingresado no v�lido. Vuelva a intentar.");
                i--;
            }
        }
        
        return respuestas;
    }
    
    public static int contarTrue(boolean[] respuestas){
        int contadorTrue=0;
        for(int i= 0; i<respuestas.length;i++){
            if(respuestas[i]==true)
                contadorTrue++;
        }
        return contadorTrue;
    }
    
    public static String obtenerAnimal(boolean[] respuestas,int cantidadTrue){
        int posicion = 0;
        switch(cantidadTrue){
            case 0 -> {
                return "Condor";
            }
            case 1 -> {
                posicion = encontrar(true,respuestas);
                if(posicion >1){
                    return "Piton";
                }else if(posicion < 1){
                    return"Caracol";
                }else{
                    return"Leon";
                }
            }
            case 2 -> {
                posicion = encontrar(false,respuestas);
                if(posicion >1){
                    return "Alse";
                }else if(posicion < 1){
                    return"Gato";
                }else{
                    return"Tortuga";
                }
            }
            case 3 -> {
                return "Caballo";
            }
            default -> {
                return null;
            }
        }
        
    }
    
    public static int encontrar(boolean valor, boolean[] respuestas){
        boolean encontrado = false;
        int posicion =0, i=0;
        while(!encontrado){
            if(respuestas[i] == valor){
                encontrado=true;
                posicion = i;
            }
            i++;
        }
        return posicion;
    }
}
