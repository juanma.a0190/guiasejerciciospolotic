package guia3;

import java.util.Scanner;

public class ClaseMain {
    
    public static void vincular(CuentaBancaria cuentaB, CuentaCorriente cuentaC, CajaAhorro cajaA){
        cuentaB.setCajaAhorro(cajaA);
        cuentaB.setCuentaCorriente(cuentaC);
    }
            

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);

        //Ejercicio 1
        //ejecutarLibro();
        //Ejercicio 2
        //ejecutarCircunferencia(leer);
        //Ejercicio 3
        ejecutarCuentaBancaria(leer);
    }

    //Ejercicio3
    public static void ejecutarCuentaBancaria(Scanner leer) {
        CuentaBancaria cb = new CuentaBancaria(500, 432540, 100);
        CuentaCorriente cuentaC = new CuentaCorriente(cb);
        CajaAhorro cajaA  = new CajaAhorro(cb);
        
        vincular(cb, cuentaC, cajaA);

        System.out.println("----------Ingreso de saldo--------------------");
        System.out.println("Ingresar saldo a depositar:");
        cb.ingresarSaldo(leer.nextDouble());

        System.out.println("----------Retiro de saldo r�pido---------------");
        System.out.println("El valor retirado del sueldo es: " + cb.extraccionRapida());

        System.out.println("----------Consultar saldo----------------------");
        System.out.println(cb.consultarSaldo());

        System.out.println("----------Consultar datos----------------------");
        System.out.println(cb.consultarDatos());

        System.out.println("----------Retiro de Saldo----------------------");
        System.out.println("Ingresar valor a retirar:");
        double valorRetiro = leer.nextDouble();
        cb.retirar(valorRetiro);
        System.out.println(cb.consultarSaldo());
        
        System.out.println("----------Retiro de Saldo Cuenta Corriente------------------");
        System.out.println("Ingrese el valor a retirar: ");
        double valor = leer.nextDouble();
        cb.getCuentaCorriente().retirar(valor);
        System.out.println(cb.consultarSaldo());
        
        System.out.println("----------Retiro de Saldo Caja Ahorro----------------------");
        cb.getCajaAhorro().retirar();
        System.out.println(cb.consultarSaldo());
        
        
        
       
    }

    //Ejercicio2
    public static void ejecutarCircunferencia(Scanner leer) {
        Circunferencia circunferencia1 = new Circunferencia();

        System.out.println("Ingrese radio:");
        circunferencia1.setRadio(leer.nextDouble());

        System.out.printf("El area es: %.2f \n", circunferencia1.calcularArea());

        System.out.printf("El perimetro es: %.2f \n", circunferencia1.calcularPerimetro());

    }

    //Ejercicio 1
    public static void ejecutarLibro() {
        Libro libro1 = new Libro();

        libro1.cargaLibro();
        libro1.mostrarLibro();
    }

    
}
