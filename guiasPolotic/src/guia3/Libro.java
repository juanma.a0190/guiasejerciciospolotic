package guia3;

import java.util.Scanner;

public class Libro {

    private String isbn;
    private String titulo;
    private String autor;
    private int numPaginas;

    public Libro() {
    }

    public Libro(String isbn, String titulo, String autor, int numPaginas) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.autor = autor;
        this.numPaginas = numPaginas;
    }
    
    public void cargaLibro(){
       Scanner leer = new Scanner(System.in);
        
        System.out.println("Ingese ISBN:");
        this.isbn = leer.nextLine();
        System.out.println("Ingrese el t�tulo:");
        this.titulo = leer.nextLine();
        System.out.println("Ingrese el autor:");
        this.autor = leer.nextLine();
        System.out.println("Ingrese el n�mero de p�ginas:");
        this.numPaginas = leer.nextInt();
    }
    
    public void mostrarLibro(){
        System.out.println(toString());
    }
    
    @Override
    public String toString(){
        return ("\nEl ISBN del libro es: "+isbn+"\nEl t�tulo es: "+titulo+"\nEl autor es: "+autor+"\n"
                + "El n�mero de p�ginas es: "+numPaginas+"\n");
    }
    
}
