package guia3;

public class CuentaBancaria {
    private int numero;
    private long dni;
    private double saldo;
    private CajaAhorro cajaAhorro;
    private CuentaCorriente cuentaCorriente;
    
    public CuentaBancaria(){
    }
    
    public CuentaBancaria(int numero, long dni, double saldo){
        this.numero = numero;
        this.dni = dni;
        this.saldo = saldo;
    }

    public CuentaBancaria(int numero, long dni, double saldo, CajaAhorro cajaAhorro, CuentaCorriente cuentaCorriente) {
        this.numero = numero;
        this.dni = dni;
        this.saldo = saldo;
        this.cajaAhorro = cajaAhorro;
        this.cuentaCorriente = cuentaCorriente;
    }
    
    
    
    public int getNumero(){
        return numero;
    }
    
    public void setNumero(int numero){
        this.numero = numero;
    }
    
    public long getDni(){
        return dni;
    }
   
    public void setDni(long dni){
        this.dni = dni;
    }
    
    public String consultarSaldo(){
        return "El saldo actual es: "+String.valueOf(saldo);
    }
    
    public void ingresarSaldo(double saldo){
        if(saldo >0){
            this.saldo += saldo;
        }
    }
   
    public double extraccionRapida(){
        final double  PORCENTAJE= 20.0;
        double importeRetirado = (this.saldo*PORCENTAJE)/100; 
        this.saldo -= importeRetirado;
        
        return importeRetirado;
    }
    
    public String consultarDatos(){
        return "El numero de la cuenta es: "+String.valueOf(numero)
                +"\nDni: "+String.valueOf(dni)
                +"\n"+this.consultarSaldo();
    }
    
    public void retirar(double retiro){
        if(retiro >0){
            this.saldo -= retiro;
            System.out.println("Se retiro correctamente: " + retiro);
        }
    }
    
    public void retirar(){
        retirar(saldo);
    }

    public CajaAhorro getCajaAhorro() {
        return cajaAhorro;
    }

    public void setCajaAhorro(CajaAhorro cajaAhorro) {
        this.cajaAhorro = cajaAhorro;
    }

    public CuentaCorriente getCuentaCorriente() {
        return cuentaCorriente;
    }

    public void setCuentaCorriente(CuentaCorriente cuentaCorriente) {
        this.cuentaCorriente = cuentaCorriente;
    }
    
    
}
