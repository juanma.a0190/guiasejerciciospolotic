package guia3;

public class Circunferencia {

    private double radio;

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public double calcularArea() {
        double area = Math.PI * Math.pow(radio, 2);
        return area;
    }
    
    public double calcularPerimetro(){
        double perimetro = 2 * Math.PI * radio;
        return perimetro;
    }

}
