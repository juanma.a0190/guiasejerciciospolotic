package guia3;

public class CuentaCorriente extends CuentaBancaria {

    CuentaBancaria cuenta;
    
    public CuentaCorriente(CuentaBancaria cuenta){
        this.cuenta = cuenta;
    }

    @Override
    public void retirar(double retiro) {
        System.out.println("Se retira de la Cuenta Corriente...");
        cuenta.retirar(retiro);
        if(retiro < 0){
            System.out.println("No se pudo retirar de la Cuenta Corriente.");
        }
    }
    
    

}
