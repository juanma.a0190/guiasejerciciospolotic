package guia3;

public class CajaAhorro extends CuentaBancaria {
    CuentaBancaria cuenta;
    
    public CajaAhorro(CuentaBancaria cuenta){
        this.cuenta = cuenta;
    }

    @Override
    public void retirar() {
        System.out.println("Se retira de la Caja de Ahorro..");
        cuenta.retirar();
    }
}
