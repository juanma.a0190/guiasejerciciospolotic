package guia1;

import java.util.Scanner;

public class Ejercicio9 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        final double DOLAR=231.68;
        final double EURO=250;
        final double GUARANI=31.00;
        final double REAL=46.81;
        
        System.out.println("Ingrese la cantidad de pesos que desea tasar: ");
        double pesos = s.nextDouble();
        
        double valorDolar = pesos/DOLAR;
        double valorEuro = pesos/EURO;
        double valorGuarani = pesos*GUARANI;
        double valorReal = pesos/REAL;
        
        System.out.printf("La cantidad de pesos ingresado %.2f equivale a: \nDolar: %.2f "
                + "\nEuro: %.2f \nGuaran�: %.2f \nReal: %.2f\n",pesos,valorDolar,valorEuro,valorGuarani,valorReal);
    }

}
