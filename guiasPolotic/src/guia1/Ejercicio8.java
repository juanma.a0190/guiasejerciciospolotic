package guia1;

import java.util.Scanner;

public class Ejercicio8 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        System.out.println("Ingrese la temperatura actual en celsius:");
        double temperaturaCelsius = s.nextDouble();
        
        double temperaturaKelvin = 253.75 + temperaturaCelsius;
        double temperaturaFahrenheit = 1.8 * temperaturaCelsius;
        /* -------------------------------------------------------------------------------
        *Mejora del c�digo:
        Se facilito la manera de formatear un double para que aparesca con dos unidades
        despu�s de la coma.
        ------------------------------------------------------------------------------- */
        System.out.printf("La temperatura en Celsius ingresada es: %.2f�C\nEn Kelvin: %.2fK\nEn Fahrenheit: %.2f�F\n",temperaturaCelsius,temperaturaKelvin,temperaturaFahrenheit);
        
    }
}
