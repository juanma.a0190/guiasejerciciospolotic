package guia1;

import java.util.Scanner;

public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        double personasEstatura[] = registroDeEstaturas(s);
        
        System.out.printf("El promedio de estaturas de las personas es: %.2f\n",calculoPromedio(personasEstatura));    
    }
    
    public static double[] registroDeEstaturas(Scanner s){
        double estaturas[] = new double[3];
        int iterador = 0;
        while(iterador < 3){
            System.out.println("Ingrese en metros la estatura de la persona " + (iterador + 1) + ":");
            estaturas[iterador] = s.nextDouble();
            if(estaturas[iterador]<=0.0 || estaturas[iterador]>=3.0){
                System.out.println("La estatura igresada no es v�lida, ingrese otra");
                iterador = 0;
                
            }else{
                iterador ++;
            }
        }
        return estaturas;
    }
    
    public static double calculoPromedio(double[] estatura){
        double suma =0, total =0;
        for (int iterador = 0; iterador < 3; iterador++) {
            suma += estatura[iterador];
        }
        total = suma / 3;
        return total;
    }
}
