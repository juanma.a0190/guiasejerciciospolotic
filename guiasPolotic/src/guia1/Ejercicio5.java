package guia1;

import java.util.Scanner;


public class Ejercicio5 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        System.out.println("Ingrese el valor de radio:");
        double radio = s.nextDouble();
       
        System.out.printf("El resultado del calculo del area es: %.2f\n",(Math.PI*Math.pow(radio, 2)));
        System.out.printf("El resultado del calculo del perimetro es: %.2f\n",(2*Math.PI*radio));
    }
    
    
}
