package guia1;

import java.util.Scanner;

public class Ejercicio6 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Ingrese el valor del producto:");
        double precioProducto = s.nextDouble();

        System.out.println("Ingrese el porcentaje de descuento:");
        double valorDescuento = s.nextDouble();

        double calculoDescuento = (precioProducto * valorDescuento) / 100;
        double totalPago = precioProducto - calculoDescuento;

        /* -------------------------------------------------------------------------------
        *Correcci�n aplicacada:
        Se informa el porcentaje del descuento y el monto a restar del valor del producto.
        
        *Mejora del c�digo:
        Se facilito la manera de formatear un double para que aparesca con dos unidades
        despu�s de la coma.
        ------------------------------------------------------------------------------- */
        System.out.printf("El descuento es del %.2f%%\n",valorDescuento);
        System.out.printf("Se resta %.2f pesos al valor del producto\n",calculoDescuento);        
        System.out.printf("EL pago total con el descuento es: %.2f pesos.\n",totalPago);
    }
}
