package guia1;
import java.util.Scanner;

public class Ejercicio7 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int aux = 0;
        
        System.out.println("Ingrese la primera edad:");
        int edad1= s.nextInt();
        
        System.out.println("Ingrese la segunda edad:");
        int edad2= s.nextInt();
        
        aux = edad1;
        edad1 = edad2;
        edad2 = aux;
        
        System.out.println("Se han intercambiado las edades \n La edad 1 quedo: "+edad1+" \n La edad 2 quedo: "+edad2);
    }
    
}
