package guia1;

import java.util.Scanner;

public class Ejercicio3 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        double numeros[] = pedidoDeNumeros(s);
        double suma = sumaDeNumeros(numeros);
        double resta = restaDeNumeros(numeros);
        double multiplicacion = multiplicacionDeNumeros(numeros);
        double division = divisionDeNumeros(numeros);

        System.out.printf("El resultado de la suma entre %.2f y %.2f  es: %.2f \n",numeros[0], numeros[1], suma);
        System.out.printf("El resultado de la resta entre %.2f y %.2f  es: %.2f \n",numeros[0], numeros[1], resta);
        System.out.printf("El resultado de la multiplicaci�n entre %.2f y %.2f  es: %.2f \n",numeros[0], numeros[1], multiplicacion);
        System.out.printf("El resultado de la divisi�n entre %.2f y %.2f  es: %.2f \n",numeros[0], numeros[1], division);
        
        /*------------------------------------------------------------------------------------------------------------------
        Ejemplo de como usar la estructura try...catch para capturar una excepci�n
        
        try{
            double division = divisionDeNumeros(numeros);
            System.out.println("El resultado de la divisi�n entre " + numeros[0] + " y " + numeros[1] + " es: " + division);
        } catch(ArithmeticException ae) {
            System.out.println("El resultado de la divisi�n es infinito");
        }
        ------------------------------------------------------------------------------------------------------------------*/

    }

    public static double[] pedidoDeNumeros(Scanner s) {
        double numeros[] = new double[2];
        for (int iterador = 0; iterador < 2; iterador++) {
            System.out.println("Ingrese el n�mero " + (iterador + 1) + ":");
            numeros[iterador] = s.nextDouble();
        }
        return numeros;
    }

    public static double sumaDeNumeros(double[] numeros) {
        double suma = numeros[0] + numeros[1];
        return suma;
    }

    public static double restaDeNumeros(double[] numeros) {
        double resta = numeros[0] - numeros[1];
        return resta;
    }

    public static double multiplicacionDeNumeros(double[] numeros) {
        double multi = numeros[0] * numeros[1];
        return multi;
    }

    public static double divisionDeNumeros(double[] numeros) {
        double division = numeros[0] / numeros[1];
        return division;
    }
}
